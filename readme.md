#### تیکتو چیست ؟ 

![1](rdmepic/1.png)

تیکتو یک وبسایت فروش بلیت سینماهای داخل کشور است که به همت 4 عضو از دانشجویان دانشگاه صنعتی خواجه نصیر الدین طوسی تهیه شده است . 

هدف از طراحی و پیاده سازی این وبسایت ارائه ی امکانات تهیه و فروش بلیت توسط شرکتهای سینمایی و تهیه ی آن توسط مردم می باشد. افراد در این پروژه می توانند بلیت سینماها و فیلم های متفاوت را خریداری کنند 

------

## بخش های مختلف تیکتو 

#### بخش توضیحات و فروش بلیت 

1. تمام فیلم های در حال اکران را در بخش اصلی وبسایت میتوانیم مشاهده و آن را خریداری کنیم   . 

همچنین اگر تمایل به خرید بلیت آن ها هستید میتوانید در صفحه ی هر فیلم اطلاعات مربوط به ساسن های مربوط به آن را مشاهده و آن را خریداری کنید .

![7](rdmepic/7.png)

<img src="rdmepic/8.png" alt="8" style="zoom:200%;" />



در هر قسمت میتوانید اطلاعات مربوط به آن فیلم شامل اطلاعات سازنده ی آن و سانس آن فیلم و سینماهای اکران کننده ی آن را مشاهده کنید .

![9](rdmepic/9.png)

![10](rdmepic/10.png)

همچنین میتوانید نظر خود را در مورد فیلم ها بیان کنید . 

### 	اطلاعات سینماها

هر یک از سینماهای موجود اطلاعات خود را برای معرفی بیشتر میتوانند به اشتراک بگذارند . 

![15](rdmepic/15.png)

![16](rdmepic/16.png)

این اطلاعات شامل آدرس های سینما و همچنین فیلم های اکران شده و در حال اکرانی است که در این سینما ها در جریان است .

![17](rdmepic/17.png)

#### پرداخت های به کاربر 

کاربر میتواند با اتصال به شبکه شتاب پرداخت خود را برای خرید بلیت انجام دهد 



![14](rdmepic/14.png)



![13](rdmepic/13.png)

#### بلاگ و اخبار مربوط به سینمای ایران 

برای اخبار مربوط به سینمای ایران و جهان قسمتی در نظر گرفته شده است که ادمین وبسایت میتواند از طریق پنل مربوط به خود ، آن را تغییر و اخبار جدید را به آن اضافه کند . 

![4](rdmepic/4.png)

همچنین جداول فروش فیلمهای ایرانی در دسترس است . 

![2](rdmepic/2.png)

#### پنل عضویت و ورود اعضا 

![12](rdmepic/12.png)



------

مشارکت کنندگان در پروژه : 

این پروژه به همت 4 نفر از دانشجویان مهندسی کامپیوتر دانشگاه صنعتی خواجه نصیر الدین طوسی پیاده سازی و طراحی شده است  . 



![moarefi](rdmepic/moarefi.jpg)



## ارتباط با ما 

Email : Mojtaba.moazen.a@gmail.com







